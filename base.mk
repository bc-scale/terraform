#
# Terragrunt Makefile
#

TERRAFORM ?= terragrunt

ALLOW_INIT_FAILURE ?= false

# macro
log = \
	tee /tmp/terraform-$@-$(shell date +%s).log

.DEFAULT_GOAL := plan

format:
	$(TERRAFORM) fmt ${EXTRA_ARGS}

fmt: format

init:
	$(TERRAFORM) init ${EXTRA_INIT_ARGS} ${EXTRA_ARGS} || $(ALLOW_INIT_FAILURE)

plan: format init
	bash -c "set -o pipefail; $(TERRAFORM) plan -input=false ${EXTRA_PLAN_ARGS} ${EXTRA_ARGS} | $(call log)"

plan-all: init
	bash -c "set -o pipefail; $(TERRAFORM) plan-all -input=false ${EXTRA_PLAN_ARGS} ${EXTRA_ARGS} | $(call log)"

plan-destroy: init
	$(TERRAFORM) plan -destroy -input=false ${EXTRA_ARGS}

apply: format init
	bash -c "set -o pipefail; $(TERRAFORM) apply -input=false ${EXTRA_APPLY_ARGS} ${EXTRA_ARGS} | $(call log)"

destroy: format plan-destroy
	$(TERRAFORM) destroy -input=false ${EXTRA_DESTROY_ARGS} ${EXTRA_ARGS}

tflint:
	tflint --config $(shell git rev-parse --show-toplevel)/.tflint.hcl

lint: tflint

tfsec:
	tfsec

sec: tfsec
