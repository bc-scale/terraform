config {
  force  = false
  module = false
}

# terraform specific
rule "terraform_comment_syntax" {
  enabled = true
}

rule "terraform_deprecated_index" {
  enabled = true
}

rule "terraform_deprecated_interpolation" {
  enabled = true
}

rule "terraform_documented_variables" {
  enabled = true
}

rule "terraform_documented_outputs" {
  enabled = true
}

rule "terraform_naming_convention" {
  enabled = true

  variable {
    custom = "^[_a-z][a-z0-9]*(_[a-z0-9]+)*$"
  }
}

rule "terraform_standard_module_structure" {
  enabled = true
}

rule "terraform_typed_variables" {
  enabled = true
}

rule "terraform_unused_declarations" {
  enabled = true
}

# aws specific
rule "aws_resource_missing_tags" {
  enabled = true

  tags = [
    "managed_by",
    "terraform_module_name",
  ]
}
