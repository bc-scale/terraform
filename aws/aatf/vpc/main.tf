module "vpc" {
  #source = "git@gitlab.com:waf-hk/terraform-modules.git//aws/vpc?ref=main"
  source = "../../../../../terraform-modules/aws/vpc"

  vpc_name         = "aatf-prod"
  cidr             = "10.0.0.0/20"
  bootstrap_bucket = "aatf-bootstrap"

  private_subnets = [
    {
      az   = "us-west-2a"
      cidr = "10.0.0.0/24"
    },
    {
      az   = "us-west-2b"
      cidr = "10.0.1.0/24"
    },
    {
      az   = "us-west-2c"
      cidr = "10.0.2.0/24"
  }]

  public_subnets = [
    {
      az   = "us-west-2a"
      cidr = "10.0.10.0/24"
    },
    {
      az   = "us-west-2b"
      cidr = "10.0.11.0/24"
    },
    {
      az   = "us-west-2c"
      cidr = "10.0.12.0/24"
  }]
}
