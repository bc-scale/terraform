module "aatf_bootstrap" {
  source = "../../../../../terraform-modules/aws/s3"

  bucket_name = "aatf-bootstrap"
}
