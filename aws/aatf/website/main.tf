module "website" {
  source = "../../../../terraform-modules/aws/website"

  bootstrap_bucket = "aatf-bootstrap"
}
